import { Component } from '@angular/core';

@Component({
  selector: 'app-home-page', // Selektor komponentu
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent {
  constructor() { }

  showSidebar() {
    const sidebar = document.querySelector('.sidebar') as HTMLElement;
    if (sidebar) {
      sidebar.style.display = 'flex';
    }
  }

  hideSidebar() {
    const sidebar = document.querySelector('.sidebar') as HTMLElement;
    if (sidebar) {
      sidebar.style.display = 'none';
    }
  }
}
