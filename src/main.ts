import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppComponent } from './app/app.component';


platformBrowserDynamic().bootstrapModule(AppComponent)
  .catch((err: any) => console.error(err));
